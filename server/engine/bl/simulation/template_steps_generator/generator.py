import pickle
from engine.view.simulation.simulation import Simulation
from engine.logs.logger import log as log
# Engines
import engine.bl.simulation.step_engine.store_mng
log.info("Building new simulations")
simulation = Simulation()


log.info("Building BaseStoreManagementSimulation")
uuid = engine.bl.simulation.step_engine.store_mng.uuid
steps_engine = engine.bl.simulation.step_engine.store_mng.StoreManagementEngine

steps_data = simulation.get_simulation_steps_data(uuid=uuid)
if not steps_data:
    log.info("New BaseStoreManagementSimulation: steps data")
    simulation_steps_data = {
        'uuid': uuid,
        'name': 'BaseStoreManagementSimulationStep',
        'description': 'Managing small store',
        'engine': pickle.dumps(steps_engine),
    }
    steps_data = simulation.create_new_simulation_steps(simulation_steps_data)
    log.info("New BaseStoreManagementSimulation: steps data: {}".format(steps_data))
else:
    log.info("Existing BaseStoreManagementSimulation steps data")

template_data = simulation.get_simulation_template_data(uuid=uuid)
if not template_data:
    simulation_template_data = {
        'uuid': uuid,
        'name': 'BaseStoreManagementSimulation',
        'data': None,
        'steps_id': str(steps_data),
        'description': 'Managing small store',
        'hierarchy': {
            'targeted_groups_id': []
            }
        }
    template_id = simulation.create_new_simulation_template(simulation_template_data)
    log.info("New BaseStoreManagementSimulation: template: {}".format(template_id))
else:
    log.info("Existing BaseStoreManagementSimulation template")
