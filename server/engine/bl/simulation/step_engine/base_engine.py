from engine.logs.logger import log as log
import datetime


class BaseEngine(object):
    def __init__(self):
        self.steps_meta = {
            'a': {'flow': ['b', 'c', 'd'], 'step_engine': lambda: None},
            'b': {'flow': ['c', 'd'], 'step_engine': lambda: None},
            'c': {'flow': ['d'], 'step_engine': lambda: None},
            'd': {'flow': [], 'step_engine': lambda: None},
        }
        self.start_step = ['a']
        self.end_step = ['c', 'd']

    def validate_engine(self):
        if not isinstance(self.steps_meta, dict):
            return False
        fields = ['flow', 'step_engine']
        for key in self.steps_meta:
            for field in fields:
                if field not in self.steps_meta[key]:
                    return False
        return True

    def get_steps_meta(self):
        if self.validate_engine():
            return self.steps_meta
        else:
            return None

    def run_step(self, current_step, all_inputs, all_states):
        engine_object = self.steps_meta[current_step]['step_engine']
        engine = engine_object()
        res = engine.run_step(all_inputs, all_states)
        if not res:
            log.error("Couldn't run simulation step: {}".format(current_step, engine_object))
        return res


class BaseSimulationStep(object):

    # ## ### ### ### ## #
    # ## INTERFACE # ## #
    # ## ### ### ### ## #

    def run_step(self, all_inputs, all_states):
        all_states = self.prepare_state(all_states)
        if self.validate_input(all_inputs[-1]['data']) and self.validate_state(all_states):
            next_state = self.calculate_step(all_inputs, all_states, all_inputs[-1]['data'])
            return next_state
        else:
            return None

    def get_name(self):
        return self.name

    def get_inputs_meta(self):
        return self.inputs_meta

    # ## ### ### ### ## #
    # ## INTERNAL ## ## #
    # ## ### ### ### ## #

    def __init__(self):
        self.name = 'No Name'
        self.inputs_meta = {}
        self.step = {}

    def calculate_step(self, all_inputs, all_states, current_inputs):
        new_state = self.step.copy()
        for key in new_state:
            if hasattr(self, self.step[key]):
                new_state[key] = getattr(self, self.step[key])(all_inputs, all_states, current_inputs)
        return new_state

    def validate_state(self, all_states):
        if all_states and 'data' in all_states[-1] and isinstance(all_states[-1]['data'], dict):
            return True
        return False

    def validate_input(self, data):
        return all(getattr(self, self.inputs_meta[key]['validation'])(data[key]) for key
                   in data if key in self.inputs_meta and
                   hasattr(self, self.inputs_meta[key]['validation']))

    def calculator_helper(self, all_inputs, all_states, current_inputs, field, action='sum'):
        if all_states and field in all_states[-1]['data']:
            res = all_states[-1]['data'][field]
        else:
            res = self.step[field]['default']
        for key in current_inputs:
            if field in current_inputs[key]['options']:
                if action == 'sum':
                    res += current_inputs[key]['options'][field]
                if action == 'multiply':
                    res *= current_inputs[key]['options'][field]
        return res

    def prepare_state(self, all_states):
        # if not all_states:
        #     all_states = [{
        #         'added': datetime.datetime.now(),
        #         'data': {}
        #     }]
        # for key in self.step:
        #     if key not in all_states[-1]['data']:
        #         all_states[-1]['data'][key] = self.step[key]['default']
        return all_states
