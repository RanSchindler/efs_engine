from engine.bl.simulation.step_engine.base_engine import BaseSimulationStep, BaseEngine

import pickle
from engine.view.simulation.simulation import Simulation


class StoreManagementEngine(BaseEngine):
    def __init__(self):
        super().__init__()
        self.steps_meta = {
            'store_management_start': {'flow': ['store_management_initialization'],
                                       'step_engine': lambda: None},
            'store_management_initialization': {'flow': ['store_management_operations'],
                                                'step_engine': lambda: None},
            'store_management_operations': {'flow': ['store_management_operations', 'store_management_improvements'],
                                            'step_engine': lambda: None},
            'store_management_improvements': {'flow': ['store_management_operations'],
                                              'step_engine': lambda: None},
            'store_management_end': {'flow': [],
                                     'step_engine': lambda: None},
        }
        self.start_step = ['store_management_start']
        self.end_step = ['store_management_end']


class BaseStoreManagementSimulationStep(BaseSimulationStep):

    def __init__(self):
        super().__init__()
        self.name = 'BaseSimulationStep.BaseStoreManagementSimulationStep'
        self.data = {
            'basic_cost': 5000
        }


class StoreManagementStart(BaseStoreManagementSimulationStep):

    def __init__(self):
        super().__init__()
        self.name = 'BaseSimulationStep.BaseStoreManagementSimulationStep.StoreManagementStart'



class StoreManagementInitialization(BaseStoreManagementSimulationStep):

    def __init__(self):
        super().__init__()
        self.name = 'BaseSimulationStep.BaseStoreManagementSimulationStep.StoreManagementInitialization'
        self.inputs_meta = {
            'store_name': {
                'validation': 'validate_company_name',
                'meta': {'file': None, 'data_object': None, 'name': 'Store Name',
                         'description': 'Choose a name for your store'}
            },
            'store_vision': {
                'validation': 'validate_company_vision',
                'meta': {'file': None, 'data_object': None, 'name': 'Store Vision',
                         'description': 'What is your vision for the store?'}
            },
            'store_size': {
                'validation': 'validate_company_size',
                'meta': {'file': None, 'data_object': None, 'name': 'Store Size',
                         'description': 'Choose a size for your store'},
                'options': {
                    'small':  {'name': 'Small',  'var_cost': 1,   'fix_cost': 0,  'purchase_chance': 1,
                               'popularity': 1, 'description': 'Space for 10 cutomers'},
                    'medium': {'name': 'Medium', 'var_cost': 1.1, 'fix_cost': 0,  'purchase_chance': 1,
                               'popularity': 1.05, 'description': 'Space for 20 cutomers'},
                    'large':  {'name': 'Large',  'var_cost': 1.2, 'fix_cost': 0,  'purchase_chance': 1,
                               'popularity': 1.1, 'description': 'Space for 30 cutomers'},
                },
            },
            'store_location': {
                'validation': 'validate_company_location',
                'meta': {'file': None, 'data_object': None, 'name': 'Store Location',
                         'description': 'Choose a location for your store'},
                'options': {
                    '1': {'name': '1', 'var_cost': 1, 'fix_cost': 0, 'purchase_chance': 1,
                          'popularity': 1, 'description': 'Normal lication'},
                    '2': {'name': '2', 'var_cost': 1.1, 'fix_cost': 0, 'purchase_chance': 1.1,
                          'popularity': 1.05, 'description': 'Good Location'},
                    '3': {'name': '3', 'var_cost': 1.2, 'fix_cost': 0, 'purchase_chance': 1.2,
                          'popularity': 1.1, 'description': 'Best Location'},
                },
            }
        }

        self.step = {
            'purchase_chance': {'calculator': 'calculate_purchase_chance', 'default': 1},  # How strong is the brand
            'popularity': {'calculator': 'calculate_popularity', 'default': 1},  # How far your are known
            'var_cost': {'calculator': 'calculate_var_cost', 'default': 0},  # How far your are known
            'fix_cost': {'calculator': 'calculate_fix_cost', 'default': 0}  # How far your are known
        }

    def calculate_purchase_chance(self, all_inputs, all_states, current_inputs):
        self.calculator_helper(all_inputs, all_states, current_inputs, 'purchase_chance', 'multiply')

    def calculate_popularity(self, all_inputs, all_states, current_inputs):
        self.calculator_helper(all_inputs, all_states, current_inputs, 'popularity', 'multiply')

    def calculate_var_cost(self, all_inputs, all_states, current_inputs):
        self.calculator_helper(all_inputs, all_states, current_inputs, 'var_cost', 'sum')

    def calculate_fix_cost(self, all_inputs, all_states, current_inputs):
        self.calculator_helper(all_inputs, all_states, current_inputs, 'fix_cost', 'sum')

    def validate_company_name(self, value):
        if not isinstance(value, str):
            return False
        if len(value) <= 0:
            return False
        return True

    def validate_company_vision(self, value):
        if not isinstance(value, str):
            return False
        if len(value) <= 0:
            return False
        return True

    def validate_company_size(self, value):
        if isinstance(value, str):
            if value.lower() not in ['small', 'medium', 'large']:
                return False
        return True

    def validate_company_location(self, value):
        if isinstance(value, str):
            if value.lower() not in ['1', '2', '3']:
                return False
        return True


class StoreManagementOperations(BaseStoreManagementSimulationStep):

    def __init__(self):
        super().__init__()
        self.name = 'BaseSimulationStep.BaseStoreManagementSimulationStep.StoreManagementOperations'


class StoreManagementImprovements(BaseStoreManagementSimulationStep):

    def __init__(self):
        super().__init__()
        self.name = 'BaseSimulationStep.BaseStoreManagementSimulationStep.StoreManagementImprovements'


class StoreManagementEnd(BaseStoreManagementSimulationStep):

    def __init__(self):
        super().__init__()
        self.name = 'BaseSimulationStep.BaseStoreManagementSimulationStep.StoreManagementEnd'


# ## ### #### ##### #### ### ## #
# Generate simulation
uuid = '4fe80400-fc72-4488-bab6-975a08ef6060'
# ## ### #### ##### #### ### ## #
