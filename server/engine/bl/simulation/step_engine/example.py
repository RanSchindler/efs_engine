from engine.bl.simulation.step_engine.base_engine import BaseSimulationStep
from engine.logs.logger import log as log


class DummyEngine(object):
    def __init__(self):
        self.steps_meta = {
            'a': {'flow': ['b', 'c', 'd'], 'step_engine': StepA},
            'b': {'flow': ['c', 'd'], 'step_engine': StepB},
            'c': {'flow': ['d'], 'step_engine': StepC},
            'd': {'flow': ['a'], 'step_engine': StepD}
        }
        self.start_step = ['a']
        self.end_step = ['d']

    def validate_engine(self):
        return True

    def get_steps_meta(self):
        return self.steps_meta

    def run_step(self, current_step, all_inputs, all_states):
        engine_object = self.steps_meta[current_step]['step_engine']
        engine = engine_object()
        res = engine.run_step(all_inputs, all_states)
        if not res:
            log.error("Couldn't run simulation step: {}".format(current_step, engine_object))
        return res


class ExampleSimulationStep(BaseSimulationStep):
    def __init__(self):
        super().__init__()
        self.inputs_meta = {
            'X': {
                'validation': 'validate_x',
                'description': 'some description',
            }
        }

        self.step = {
            'branding': 'calculate_branding',  # How strong is the brand
            'popularity': 'calculate_popularity',  # How far your are known
        }

    def calculate_branding(self, all_inputs, all_states, current_inputs):
        return 1

    def calculate_popularity(self, all_inputs, all_states, current_inputs):
        return 1

    def validate_x(self, value=None):
        if not value:
            return False
        if not isinstance(value, str):
            return False
        if len(value) < 10:
            return False
        return True


class StepA(ExampleSimulationStep):
    def __init__(self):
        super().__init__()
        self.name = 'StepA'


class StepB(ExampleSimulationStep):
    def __init__(self):
        super().__init__()
        self.name = 'StepB'


class StepC(ExampleSimulationStep):
    def __init__(self):
        super().__init__()
        self.name = 'StepC'


class StepD(ExampleSimulationStep):
    def __init__(self):
        super().__init__()
        self.name = 'StepC'



