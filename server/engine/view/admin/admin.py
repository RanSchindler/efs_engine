from engine.logs.logger import admin as admin_log
from engine.db.manager import Manager as dbManager
import hashlib
from bson.objectid import ObjectId


class Admin(object):

    def __init__(self):
        self.db_manager = dbManager()
        self.pre_salt = 'Angie, Angie When will those dark clouds all disappear'
        self.post_salt = 'Angie, Angie Where will it lead us from here With' \
                         ' no lovin in our souls And no money in our coats'
        self.hierarchy_base = {
                'type': None,
                'region': None,
                'country': None,
                'state': None,
                'city': None,
                'zone': None,
                'grade': None,
                'class': None,
                'year': None,
                'month': None,
                'iteration': None
        }

    def create_new_user(self, user_data):
        if self.validate_user_data(user_data):
            fixed_user_data = self.prepare_new_user_data(user_data)
            admin_log.info('creating new user: {}'.format(fixed_user_data))
            db_data_manager = self.db_manager.get_data_manager()
            result = db_data_manager.save('user', fixed_user_data)
            admin_log.info('Added new user. ID: {}'.format(result.inserted_id))
            self.create_user_token(str(result.inserted_id))
        else:
            fixed_user_data = self.strip_sensitive_data(user_data)
            admin_log.error('Failed Adding new user: {}'.format(fixed_user_data))

    def create_new_targeted_group(self, targeted_group_data):
        if self.validate_targeted_group_data(targeted_group_data):
            admin_log.info('creating new group: {}'.format(targeted_group_data))
            result = self.db_manager.get_data_manager().save('targeted_group', targeted_group_data)
            if result:
                admin_log.info('Added new targeted group ID: {}'.format(result.inserted_id))
                return result.inserted_id
        admin_log.error('Failed Adding new targeted group: {}'.format(targeted_group_data))
        return None

    def validate_targeted_group_data(self, targeted_group_data):
        return True

    def change_user_targeted_group(self, user_id, targeted_group_id, action='add'):
        targeted_group = self.get_targeted_group_data(targeted_group_id)

        if action == 'add':
            if user_id not in targeted_group['users']:
                targeted_group['users'].append(user_id)

        if action == 'remove':
            if user_id in targeted_group['users']:
                targeted_group['users'].remove(user_id)

        search = {}
        search.update({'_id': targeted_group['_id']})

        result = self.db_manager.get_data_manager().update('targeted_group', search,
                                                           {'users': targeted_group['users']})
        if self.db_manager.get_data_manager().verify_write_result(result):
            admin_log.info('Update targeted group {} with user: {}'.format(targeted_group_id, user_id))
            return True
        else:
            admin_log.info('Failed to Update targeted group {} with user: {}'.format(targeted_group_id, user_id))
            return False

    def get_targeted_group_data(self, _id):
        search = {}
        search.update({'_id': ObjectId(_id)})

        result = self.db_manager.get_data_manager().find_one('targeted_group', search)
        if result:
            admin_log.info('Found targeted group: {}'.format(result))
            return result
        else:
            admin_log.info("Didn't find targeted group: {}".format(search))
            return None

    def create_god_targeted_group(self):
        admin_log.info('Creating god targeted group')
        targeted_group_data = {
            'name': 'MBA Mars 2018',
            'lead': None,
            'users': ['5a50c4335f5d04135c82c989']
        }
        return self.create_new_targeted_group(targeted_group_data)

    def create_god_group(self):
        admin_log.info('Creating god group')
        group_data = {
            'name': 'some name',
            'lead': None,
            'users': []
        }
        return self.create_new_group(group_data)

    def create_god(self):
        admin_log.info('Creating god user')
        user_data = {
            'hierarchy': self.hierarchy_base,
            'user_name': 'god',
            'password': 'user',
            'a_password': 'admin',
            's_password': 'super',
            'g_password': 'god',
            'mail': 'this.is@gmail.com',
            # 'high_roles': {
            #     'god': True,
            #     'super': True,
            # },
            'roles': {
                'admin': {'all': True,
                          'group': {
                              'all': True,
                              'add': True,
                              'remove': True
                           },
                          'targeted_group': {
                              'all': True,
                              'add': True,
                              'remove': True
                           },
                          },
                'user': {'all': True},
                'targeted_groups': ['5a5103db6738ec07e0858fec', '5a5103c76738ec07e0858feb', '5a50c4335f5d04135c82c989']
            },
        }
        self.create_new_user(user_data)

    def strip_sensitive_data(self, user_data):
        fields = ['password', 'a_password', 's_password', 'g_password']
        for field in fields:
            if field in user_data:
                del user_data[field]
        return user_data

    ''' Create token for user supplied by user_id '''
    def create_user_token(self, user_id):
        salted_user_id = self.pre_salt + str(user_id) + self.post_salt
        token_object = {
            'user_id': user_id,
            'token': hashlib.sha512(str(salted_user_id).encode('utf-8')).hexdigest()
        }
        admin_log.info('Created new user token data: {}'.format(token_object))
        db_data_manager = self.db_manager.get_data_manager()
        result = db_data_manager.save('user_token', token_object)
        admin_log.info('Added new user token. token ID: {} Token: {} User ID {}'.format(
            result.inserted_id, token_object['token'], user_id))

    def validate_user_data(self, user_data):
        res = True

        mandatory_user_data = {
            'user_name': True,
            'password': True,
            'mail': True,
        }
        for field in mandatory_user_data.keys():
            if field not in user_data:
                res = False
        if 'user_name' in user_data and 'mail' in user_data:
            search = {
                'user_name': user_data['user_name'],
                'mail': user_data['mail']
            }
            db_data_manager = self.db_manager.get_data_manager()
            result = db_data_manager.find_one('user', search)
            if result:
                res = False

        return res

    ''' Fix user data such as passwords '''
    def prepare_new_user_data(self, user_data):
        fields = ['password', 'a_password', 's_password', 'g_password']
        for field in fields:
            if field in user_data:
                user_data[field] = hashlib.sha512(str(user_data[field]).encode('utf-8')).hexdigest()

        # remove high_roles just to be safe
        if 'high_roles' in user_data:
            del user_data['high_roles']

            # Set basic role if empty
        if 'roles' not in user_data:
            user_data['roles'] = {}
        if 'user' not in user_data['roles']:
            user_data['roles']['user'] = {}
        if not user_data['roles']['user']:
            user_data['roles']['user'] = {'all': True}

        return user_data

    def get_god_id(self):
        search = {
            'user_name': 'god',
        }
        db_data_manager = self.db_manager.get_data_manager()
        result = db_data_manager.find_one('user', search, {'_id':1})
        return result['_id']

    def validate_group_data(self, group_data):
        return bool(group_data)

    def create_new_group(self, group_data):
        if self.validate_group_data(group_data):
            admin_log.info('creating new group: {}'.format(group_data))
            result = self.db_manager.get_data_manager().save('group', group_data)
            if result:
                admin_log.info('Added new group ID: {}'.format(result.inserted_id))
                return result.inserted_id
        admin_log.error('Failed Adding new group: {}'.format(group_data))
        return None

    def get_group_data(self, _id):
        search = {}
        search.update({'_id': ObjectId(_id)})

        result = self.db_manager.get_data_manager().find_one('group', search)
        if result:
            admin_log.info('Found group: {}'.format(result))
            return result
        else:
            admin_log.info("Didn't find group: {}".format(search))
            return None

    def change_user_group(self, user_id, group_id, action='add'):
        group = self.get_group_data(group_id)

        if action == 'add':
            if user_id not in group['users']:
                group['users'].append(user_id)

        if action == 'remove':
            if user_id in group['users']:
                group['users'].remove(user_id)

        search = {}
        search.update({'_id': group['_id']})

        result = self.db_manager.get_data_manager().update('group', search, {'group': group['users']})
        if self.db_manager.get_data_manager().verify_write_result(result):
            admin_log.info('Update group {} with user: {}'.format(group_id, user_id))
            return True
        else:
            admin_log.info('Failed to Update group {} with user: {}'.format(group_id, user_id))
            return False

    def get_all_users(self, hierarchy):
        search = {'hierarchy': {x: hierarchy[x] for x in hierarchy if x in self.hierarchy_base}}
        result = self.db_manager.get_data_manager().find('user', search)
        if result:
            admin_log.info('Found users: {}'.format(result))
            return list(result)
        else:
            admin_log.info("Didn't find users: {}".format(search))
            return None

    def get_all_users_group(self, users):
        search = {'users': {'$in': users}}
        result = self.db_manager.get_data_manager().find('group', search)
        if result:
            admin_log.info('Found groups: {}'.format(result))
            return list(result)
        else:
            admin_log.info("Didn't find groups: {}".format(search))
            return None

    def get_all_users_simulations(self, users=None, groups=None):
        users = users or []
        groups = groups or []

        search = {'$or': [
            {'user': {'$in': users}},
            {'group': {'$in': groups}},
        ]}

        result = self.db_manager.get_data_manager().find('simulation', search)
        if result:
            admin_log.info('Found groups: {}'.format(result))
            return list(result)
        else:
            admin_log.info("Didn't find groups: {}".format(search))
            return None

    def get_all_targeted_groups(self, user_id=None):

        search = {'users': user_id}

        result = self.db_manager.get_data_manager().find('targeted_group', search)
        if result:
            admin_log.info('Found targeted groups: {}'.format(result))
            return list(result)
        else:
            admin_log.info("Didn't find targeted groups: {}".format(search))
            return None
