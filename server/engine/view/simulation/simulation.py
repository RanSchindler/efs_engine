from engine.db.manager import Manager as dbManager
from engine.logs.logger import log as log
import pickle
from bson.objectid import ObjectId
import datetime


class Simulation(object):

    def __init__(self):
        self.db_manager = dbManager()

    def create_new_simulation(self, simulation_data):
        if self.validate_simulation_data(simulation_data):
            simulation_data = self.prepare_simulation_for_saving(simulation_data)
            log.info('creating new simulation: {}'.format(simulation_data))
            result = self.db_manager.get_data_manager().save('simulation', simulation_data)
            log.info('Added new simulation. ID: {}'.format(result.inserted_id))
            return result.inserted_id

    def create_new_simulation_template(self, simulation_template_data):
        if self.validate_simulation_template_data(simulation_template_data):
            simulation_template_data = self.prepare_simulation_template_for_saving(simulation_template_data)
            log.info('creating new simulation template: {}'.format(simulation_template_data))
            result = self.db_manager.get_data_manager().save('simulation_template', simulation_template_data)
            log.info('Added new simulation template. ID: {}'.format(result.inserted_id))
            return result.inserted_id

    def create_new_simulation_steps(self, simulation_steps_data):
        if self.validate_simulation_steps_data(simulation_steps_data):
            simulation_steps_data = self.prepare_simulation_steps_for_saving(simulation_steps_data)
            log.info('creating new simulation steps: {}'.format(simulation_steps_data))
            result = self.db_manager.get_data_manager().save('simulation_steps', simulation_steps_data)
            log.info('Added new simulation steps. ID: {}'.format(result.inserted_id))
            return result.inserted_id

    def prepare_simulation_for_saving(self, simulation_data):
        base_fields = {
            'state': {},  # [data]
            'input': {
                'saved': {},  # [data]
                'submitted': {},  # [data]
            },
            'template': None,
            'data': None,
            'roles': {
                'all': {
                    'read': True,
                    'submit': False
                }
            }
        }

        for key in base_fields:
            if key not in simulation_data:
                simulation_data[key] = base_fields[key]

        return simulation_data

    def prepare_simulation_template_for_saving(self, simulation_template_data):
        return simulation_template_data

    def prepare_simulation_steps_for_saving(self, simulation_steps_data):
        return simulation_steps_data

    def validate_simulation_data(self, simulation_data):
        if not any([simulation_data['user'], simulation_data['group']]):
            return False
        return True

    def validate_simulation_template_data(self, simulation_template_data):
        return True

    def validate_simulation_steps_data(self, simulation_steps_data):
        return True

    def get_simulation_data(self, _id=None, user=None, group=None, uuid=None):
        search = {}
        search.update({'_id': ObjectId(_id)}) if _id else None
        search.update({'user': user}) if user else None
        search.update({'group': group}) if group else None
        search.update({'uuid': uuid}) if uuid else None

        result = self.db_manager.get_data_manager().find_one('simulation', search)
        if result:
            log.info('Found Simulation: {}'.format(result))
            return result
        else:
            log.info("Didn't find Simulation: {}".format(search))
            return None

    def get_simulation_template_data(self, _id=None, name=None, uuid=None):
        search = {}
        search.update({'_id': ObjectId(_id)}) if _id else None
        search.update({'name': name}) if name else None
        search.update({'uuid': uuid}) if uuid else None

        result = self.db_manager.get_data_manager().find_one('simulation_template', search)
        if result:
            log.info('Found Simulation template: {}'.format(result))
            return result
        else:
            log.info("Didn't find Simulation template: {}".format(search))
            return None

    def get_all_simulation_templates(self, targeted_groups=None):
        targeted_groups = targeted_groups or []

        search = {'hierarchy.targeted_groups_id': {'$in': targeted_groups}}

        result = self.db_manager.get_data_manager().find('simulation_template', search)
        if result:
            log.info('Found simulation templates: {}'.format(result))
            return list(result)
        else:
            log.info("Didn't find simulation templates: {}".format(search))
            return None

    def get_simulation_steps_data(self, _id=None, name=None, uuid=None):
        search = {}
        search.update({'_id': ObjectId(_id)}) if _id else None
        search.update({'name': name}) if name else None
        search.update({'uuid': uuid}) if uuid else None

        result = self.db_manager.get_data_manager().find_one('simulation_steps', search)
        if result:
            log.info('Found Simulation steps: {}'.format(result))
            return result
        else:
            log.info("Didn't find Simulation steps: {}".format(search))
            return None

    def get_simulation_steps_id(self, simulation_id):
        template_id = self.get_simulation_template_id(simulation_id)
        template = self.get_simulation_template_data(template_id)
        steps_id = template['steps_id']
        return steps_id

    def get_simulation_template_id(self, simulation_id):
        simulation = self.get_simulation_data(simulation_id)
        template_id = simulation['template_id']
        return template_id

    def get_simulation_state(self, simulation_id):
        simulation = self.get_simulation_data(simulation_id)
        return simulation['state']

    def get_simulation_step_engine(self, simulation_id):
        simulation_step_id = self.get_simulation_steps_id(simulation_id)
        step = self.get_simulation_steps_data(simulation_step_id)
        engine = pickle.loads(step['engine'])()
        return engine

    def save_simulation_input(self, simulation_id, input_data):
        simulation = self.get_simulation_data(simulation_id)
        new_data = {
            'added': datetime.datetime.now(),
            'data': input_data
        }
        simulation['input']['saved'].append(new_data)

        search = {}
        search.update({'_id': simulation['_id']})

        result = self.db_manager.get_data_manager().update('simulation', search,
                                                           {'input.saved': simulation['input']['saved']})
        if self.db_manager.get_data_manager().verify_write_result(result):
            log.info('Update simulation with new data: {}'.format(new_data))
            return True
        else:
            log.info('Failed to Update simulation with new data: {}'.format(new_data))
            return False

    def save_simulation_state(self, simulation_id, state):
        simulation = self.get_simulation_data(simulation_id)
        new_data = {
            'added': datetime.datetime.now(),
            'data': state
        }
        simulation['state'].append(new_data)

        search = {}
        search.update({'_id': simulation['_id']})

        result = self.db_manager.get_data_manager().update('simulation', search,
                                                           {'state': simulation['state']})
        if self.db_manager.get_data_manager().verify_write_result(result):
            log.info('Update simulation {} with new state: {}'.format(simulation_id, new_data))
            return True
        else:
            log.info('Failed to Update simulation {} with new state: {}'.format(simulation_id, new_data))
            return False

    def submit_simulation_step(self, simulation_id):
        simulation = self.get_simulation_data(simulation_id)
        simulation['input']['submitted'].append({
            'submit': datetime.datetime.now(),
            'added': simulation['input']['saved'][-1]['added'],
            'data': simulation['input']['saved'][-1]['data']
        })

        search = {}
        search.update({'_id': simulation['_id']})

        result = self.db_manager.get_data_manager().update('simulation', search,
                                                           {'input.submitted': simulation['input']['submitted']})
        if self.db_manager.get_data_manager().verify_write_result(result):
            log.info('Update simulation {} state with latest input  data: {}'.format(
                simulation_id, simulation['input']['submitted'][-1]))
            return True
        else:
            log.info('Failed to Update simulation {} state with latest input  data: {}'.format(
                simulation_id, simulation['input']['submitted'][-1]))
            return False

    def validate_simulation_step_input(self, simulation_id, input_data):
        simulation = self.get_simulation_data(simulation_id)
        simulation_step_id = self.get_simulation_steps_id(simulation_id)
        step = self.get_simulation_steps_data(simulation_step_id)
        engine = pickle.loads(step['engine'])()
        current_step = simulation['data']['current_step']
        step_engine = engine.get_steps_meta()[current_step]['step_engine']()
        return step_engine.validate_input(input_data)

    def get_simulation_next_steps(self, simulation_id):
        simulation = self.get_simulation_data(simulation_id)
        engine = self.get_simulation_step_engine(simulation_id)
        current_step = simulation['data']['current_step']
        return engine.get_steps_meta()[current_step]['flow']

    def set_simulation_next_steps(self, simulation_id, step_name=None):
        simulation = self.get_simulation_data(simulation_id)

        search = {}
        search.update({'_id': simulation['_id']})

        result = self.db_manager.get_data_manager().update('simulation', search, {'data.current_step': step_name})
        if self.db_manager.get_data_manager().verify_write_result(result):
            log.info('Update simulation {} current step name: {}'.format(simulation_id, step_name))
            return True
        else:
            log.info('Failed to Update simulation {} current step name: {}'.format(simulation_id, step_name))
            return False

    def run_simulation_step(self, simulation_id):
        simulation = self.get_simulation_data(simulation_id)
        current_step = simulation['data']['current_step']
        all_inputs = simulation['input']['submitted']
        all_states = simulation['state']

        engine = self.get_simulation_step_engine(simulation_id)
        next_state = engine.run_step(current_step, all_inputs, all_states)
        res = self.save_simulation_state(simulation_id, next_state)
        if res:
            log.info('run_simulation_step {} and saves new state: {}'.format(simulation_id, next_state))
            return True
        else:
            log.info('Failed to run_simulation_step {} and saves new state: {}'.format(simulation_id, next_state))
            return False

    def set_simulation_roles(self, simulation_id, roles):
        simulation = self.get_simulation_data(simulation_id)

        search = {}
        search.update({'_id': simulation['_id']})

        result = self.db_manager.get_data_manager().update('simulation', search, {'roles': roles})
        if self.db_manager.get_data_manager().verify_write_result(result):
            log.info('Update simulation {} roles: {}'.format(simulation_id, roles))
            return True
        else:
            log.info('Failed to Update simulation {} roles: {}'.format(simulation_id, roles))
            return False

    def get_simulation_roles(self, simulation_id):
        simulation = self.get_simulation_data(simulation_id)
        return simulation['roles']

    def create_empty_steps(self):
        from engine.bl.simulation.step_engine.example import DummyEngine

        log.info('Creating an empty simulation steps')
        simulation_steps_data = {
            'name': 'dummy_simulation_steps',
            'description': None,
            'engine': pickle.dumps(DummyEngine),
        }
        self.create_new_simulation_steps(simulation_steps_data)

    def create_empty_simulation_template(self):
        log.info('Creating an empty simulation template')
        simulation_steps = self.get_simulation_steps_data(name='dummy_simulation_steps')
        simulation_template_data = {
            'name': 'dummy_simulation_template',
            'data': None,
            'steps_id': str(simulation_steps['_id']),
            'description': None,
            'hierarchy': {
                'targeted_groups_id': ['5a5103db6738ec07e0858fec'],  # [5a5103db6738ec07e0858fec]
            }
        }
        self.create_new_simulation_template(simulation_template_data)

    def create_empty_simulation(self, user):
        log.info('Creating an empty simulation')
        simulation_template_data = self.get_simulation_template_data(name='dummy_simulation_template')
        simulation_data = {
            'user': user,
            'group': None,
            'state': [],
            'input': {
                'saved': [],
                'submitted': [],
            },
            'template_id': str(simulation_template_data['_id']),
            'data': {'current_step': 'a'},
            'roles': {
                'all': {
                    'read': True,
                    'submit': False
                },
                user: {
                    'read': True,
                    'submit': True
                }
            }
        }
        self.create_new_simulation(simulation_data)

# ## ### #### ##### ###### ####### ###### ##### #### ### ## #
# Example ### ##### ###### ####### ###### ##### #### ### ## #
# ## ### #### ##### ###### ####### ###### ##### #### ### ## #



