from engine.db.manager import Manager as dbManager
from bson.objectid import ObjectId
from engine.logs.logger import log as log
import hashlib


class User(object):

    def __init__(self):
        self.db_manager = dbManager()

    def get_user_token(self, user_id):
        search = {"user_id": user_id}
        db_data_manager = self.db_manager.get_data_manager()
        result = db_data_manager.find_one('user_token', search, {'_id': 0, 'token': 1})
        log.info('Found user token: {}'.format(result))
        return result

    def get_user_data(self, user_id):
        search = {"_id": ObjectId(user_id)}
        db_data_manager = self.db_manager.get_data_manager()
        result = db_data_manager.find_one('user', search, {'_id': 0, 'password': 0, 'a_password': 0, 's_password': 0,
                                                           'g_password': 0})
        if result:
            log.info('Found user Data: {}'.format(result))
            return result
        else:
            log.info("Didn't find user Data: {}".format(search))
            return None

    def auth_user(self, user_name, password):
        search = {
            "user_name": user_name,
            "password": hashlib.sha512(str(password).encode('utf-8')).hexdigest()
        }
        db_data_manager = self.db_manager.get_data_manager()
        result = db_data_manager.find_one('user', search, {'_id': 1})
        if result:
            log.info('Auth User: {}'.format(result))
            return str(result['_id'])
        else:
            log.info("Couldn't Auth User. probably wrong user name ot password: {}".format(user_name))

    def login(self, user_id):
        log.info('Login: {}'.format(user_id))

    def logout(self, user_id):
        log.info('Logout: {}'.format(user_id))

    def get_user_simulations(self, user=None, group=None):
        search = {}
        search.update({'user': user}) if user else None
        search.update({'group': group}) if group else None

        projection = {'_id': 1, 'user': 1, 'group': 1}

        result = self.db_manager.get_data_manager().find('simulation', search, projection)
        if result:
            log.info('Found Simulation: {}'.format(result))
            return list(result)
        else:
            log.info("Didn't find Simulations: {}".format(search))
            return None
