import logging.handlers
import os

maxBytes = 1024*1024*10

log = logging.getLogger('log')
log.setLevel(logging.DEBUG)
log.addHandler(logging.handlers.RotatingFileHandler(os.path.dirname(__file__)+'/output/log.txt',
                                                    maxBytes=maxBytes, backupCount=5))

auth = logging.getLogger('auth')
auth.setLevel(logging.DEBUG)
auth.addHandler(logging.handlers.RotatingFileHandler(os.path.dirname(__file__)+'/output/auth.txt',
                                                     maxBytes=maxBytes, backupCount=5))

admin = logging.getLogger('admin')
admin.setLevel(logging.DEBUG)
admin.addHandler(logging.handlers.RotatingFileHandler(os.path.dirname(__file__)+'/output/admin.txt',
                                                      maxBytes=maxBytes, backupCount=5))

testings = logging.getLogger('testings')
testings.setLevel(logging.DEBUG)
testings.addHandler(logging.handlers.RotatingFileHandler(os.path.dirname(__file__)+'/output/testings.txt',
                                                         maxBytes=maxBytes, backupCount=5))

info = log.info
error = log.error
warn = log.warning
debug = log.debug

auth_info = auth.info
auth_error = auth.error
auth_warn = auth.warning
auth_debug = auth.debug

admin_info = admin.info
admin_error = admin.error
admin_warn = admin.warning
admin_debug = admin.debug

testings_info = testings.info
testings_error = testings.error
testings_warn = testings.warning
testings_debug = testings.debug
