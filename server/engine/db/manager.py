from pymongo import MongoClient
from bson.objectid import ObjectId

class MongoDBManager(object):
    def __init__(self):
        self.client = None
        self.db = None
        self.DB = 'ecofinsim'
        self.ip = 'localhost'
        self.port = 27017

        self.setup_connection()

    def setup_connection(self):
        self.set_client()
        self.set_db()

    def set_client(self):
        self.client = MongoClient(self.ip, self.port)

    def set_db(self):
        self.db = self.client[self.DB]

    def get_collection(self, collection):
        return self.db[collection]

    def save(self, collection, data):
        return self.get_collection(collection).insert_one(data)

    def update(self, collection, query, data):
        data = {'$set': data}
        return self.get_collection(collection).update_one(query, data)

    def replace(self, collection, query, data):
        return self.get_collection(collection).update_one(query, data)

    def find_one(self, collection, data, projection=None):
        return self.get_collection(collection).find_one(data, projection)

    def find(self, collection, data, projection=None):
        return self.get_collection(collection).find(data, projection)

    def find_one_by_id(self, collection, _id):
        return self.get_collection(collection).find_one({"_id": ObjectId(_id)})

    def verify_write_result(self, result):
        return result.acknowledged and result.matched_count == 1 and result.modified_count == 1


class Manager(object):
    def __init__(self):
        self.mongo_db_manager = None

    def get_data_manager(self):
        return self.get_mongodb_manager()

    def get_mongodb_manager(self):
        if not self.mongo_db_manager:
            self.mongo_db_manager = MongoDBManager()
        return self.mongo_db_manager

