import engine.view.admin.admin as admin
import engine.view.user.user as user
import engine.view.simulation.simulation as simulation

a = admin.Admin()
a.create_god()  # Will create only one
group_id = a.create_god_group()
targeted_group_id = a.create_god_targeted_group()

u = user.User()
user_id = u.auth_user('god', 'user')
a.change_user_group(str(user_id), group_id, 'add')
print(u.get_user_token(user_id))
print(u.get_user_data(user_id))

s = simulation.Simulation()
s.create_empty_steps()
s.create_empty_simulation_template()
s.create_empty_simulation(user_id)
